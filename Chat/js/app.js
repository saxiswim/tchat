
//ajax permettant de récupérer le JSON des messages et de les affichers dynamiquement


function getMessages(){
  // créer une requête AJAX pour se connecter au serveur(au fichier handler.php en particuliezr)
  const requeteAjax = new XMLHttpRequest();
  requeteAjax.open("GET", "handler.php");
  // quand elle reçoit les données, il faut qu'elle les traite (en exploitant le JSON) et il faut qu'elle affiche ces données au format HTML
  //la date ne fonctionne pas, a recheck
  requeteAjax.onload = function(){
    const resultat = JSON.parse(requeteAjax.responseText);
    const html = resultat.reverse().map(function(message){
      return `
        <div class="message"> 
          <span class="date">${message.created_at.substring(11, 16)}</span>
          <strong><span class="author">${message.author}</span> : </strong>
          <span class="content">${message.content}</span>
        </div>
      `
    }).join('');

    const messages = document.querySelector('.messages');

    messages.innerHTML = html;
    messages.scrollTop = messages.scrollHeight;
  }

  // on envoie la requête
  requeteAjax.send();
}



function postMessage(event){
  //stop le submit du formulaire
  event.preventDefault();




  //  récupére les données du formulaire
  //------------------commenter la ligne ci dessous si on veut mettre l'utilisateur à la main --------

  //const author = document.querySelector('#author');// on utilisera  $_SESSION['login']; quand l'on aura mis en place les sessions
  const content = document.querySelector('#content');

  //  conditionne les données
  const data = new FormData();
  //data.append('author', author.value);
  data.append('content', content.value);

  // configure une requête ajax en POST et envoyer les données
  const requeteAjax = new XMLHttpRequest();
  requeteAjax.open('POST', 'handler.php?task=write');
  
  requeteAjax.onload = function(){
    content.value = '';
    content.focus();
    getMessages();
  }

  requeteAjax.send(data);
}

document.querySelector('form').addEventListener('submit', postMessage);

//rafraichissement toutes lkes 3sec
const interval = window.setInterval(getMessages, 3000);

getMessages();