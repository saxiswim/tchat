<?php

/*ce fichier php permet la connexion à la base de donnée ainsi que l'accession à tous les champs utile
c'est dans cette partie que si du code concernant les données contenu dans les bases doivent etre ajouté*/

class  DatabaseConnection{
    private $pdo;


    public function __construct()
    {
        $dsn = "mysql:host=localhost;dbname=projet_dev_web";
        $username = "nom_utilisateur_choisi";
        $password ="mot_de_passe_solide";
        try
        {
            $this->pdo = new PDO($dsn,$username,$password);
            $this->pdo->setAttribute(PDO::ATTR_ERRMODE,PDO::ERRMODE_WARNING);
        }
        catch (Exception $e)
        {
            $this->pdo->setAttribute(PDO::ATTR_ERRMODE,PDO::ERRMODE_WARNING);
            die('Erreur:' . $e->getMessage());
        }
    }

    public function closePDO ()
    {
        $this->pdo = null;
    }

    /**
     * @return PDO
     */
    public function getPdo()
    {
        return $this->pdo;
    }

    /**
     * @param PDO $pdo
     */
    /*public function setPdo($pdo)
    {
        $this->pdo = $pdo;
    }*/



}






