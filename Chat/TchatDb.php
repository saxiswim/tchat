<?php

require_once 'DatabaseConnection.php';

class TchatDb
{
    private $bd;
    public $id_partie;

    public function __construct($id)
    {
        $this->id_partie = $id;
        $this->bd = new DatabaseConnection();
    }

    public function getmessage()
    {
        $res = $this->bd->getPdo()->query("SELECT * FROM chat WHERE Id_partie = '".$this->getid()."' ORDER BY created_at DESC LIMIT 20");
        return $res->fetchAll();
    }

    public function postmessage($author,$content)
    {
        $req = $this->bd->getPdo()->prepare('INSERT INTO chat SET Id_partie = :Id_partie , author = :author, content = :content, created_at = NOW()');
        $req->execute(["Id_partie" => $this->id_partie, "author" => $author,"content" => $content]);
        $this->bd->closePDO();
    }

    public function getid()
    {
        return $this->id_partie;
    }
}

